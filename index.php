<html>

<head>

    <link rel="stylesheet" type="text/css" href="./style.css">

</head>

<body>

    <h1>Pendu</h1>

    <button><a href="new-partie.php">Nouvelle Partie</a></button>

    <?php require 'header.php';

    echo '<ul>';

    try {

        $connexion = new PDO("mysql:host=$hostname;dbname=$database", $username, $password);

        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = 'select * from partie';

        foreach ($connexion->query($sql) as $row) {

            if ($row['victoire'] == 2)

                echo "<li><strong>" . $row['nom_joueur1'] . '</strong> vs ' . $row['nom_joueur2'] . " : " . $row['mot'] . ' (' . $row['nb_coup'] . " coups) </li>";

            else

                echo "<li>" . $row['nom_joueur1'] . ' vs <strong>' . $row['nom_joueur2'] . "</strong> : " . $row['mot'] . " </li>";
        }
    } catch (PDOException $e) {

        die('Erreur PDO : ' . $e->getMessage());
    } catch (Exception $e) {

        die('Erreur générale : ' . $e->getMessage());
    }

    echo '</ul>';

    ?>

</body>

</html>
