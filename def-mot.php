<html>

<head>

    <link rel="stylesheet" type="text/css" href="./style.css">

</head>

<body>

    <h1><?php require 'header.php';
        echo $_SESSION['joueur1']; ?> choisit un mot</h1>

    <form method="get">

        <input name="solution" autofocus="autofocus"></input>

        <input type="submit" value="suivant">

    </form>

    <?php //pas de require car déjà fait au dessus

    if (isset($_GET['solution']) && !empty($_GET['solution'])) {

        $_SESSION['solution'] = $_GET['solution'];

        header('Location: jeu.php?traitement=OK'); //redirection

        exit(0); //On « tue » le script pour ne pas aller plus loin

    }

    ?>

</body>

</html>
