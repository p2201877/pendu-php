# Pendu

Pendu en PHP jouable à 2 joueurs avec tableau des scores. Créé à l'IUT dans le cadre du cours de PHP.

## Requirements

Serveur web, base de données, php

ex : 
```bash
apt install apache2 mysql-server php php-mysqli -y
```

## Installation

 - déplacer les fichiers php dans le répertoire du serveur web

ex : 
```bash
rm /var/www/html
mv *.php /var/www/html
mv *.css /var/www/html
```

 - Créer la base de données 

ex : 
```bash
systemctl start mysql
mysql
CREATE DATABASE pendu;
use pendu
source database.sql
exit
```

 - Ajouter les identifiants d'un utilisateur ayant accès à la base de données dans **header.php**


 - Démarrer le serveur web

ex : 
```bash
systemctl start apache2
```

## Images

![image](pendu.png)

## Améliorations possibles

 - empêcher l'utilisateur d'entrer une chaine vide
 - ajouter d'images (photo de profil)
 - utiliser les mots de la table lexique
 - pouvoir choisir le nombre de coups maximum
