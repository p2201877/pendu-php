<html>

<head>

    <link rel="stylesheet" type="text/css" href="./style.css">

    <link rel="stylesheet" type="text/css" href="./jeu.css">

</head>

<body>

    <form method="get">

        <input name="mot" id="mot" autofocus="autofocus"></input>

        <input type="submit" value="ok">

    </form>

    <?php require 'header.php';

    echo '<h1>' . $_SESSION['joueur2'] . ' devine le mot</h1>';

    if (isset($_GET['mot']) && !empty($_GET['mot'])) {

        //compteur d'essais

        if (isset($_SESSION['nb_essais']))

            $_SESSION['nb_essais'] += 1;

        else

            $_SESSION['nb_essais'] = 1; // on le met à 1 pour le premier essai

        echo '<p>essai : ' . $_SESSION["nb_essais"] . '</p>';



        //test des lettres

        $_SESSION['mot_trouve'] = true;

        $_SESSION['deja_dites'][$_SESSION['nb_essais']] = $_GET['mot'];

        echo "<p>lettre tentée : " . $_SESSION['deja_dites'][$_SESSION['nb_essais']] . "</p>";

        $au_moins_une_lettre_trouvee = false;

        for ($i = 0; $i < strlen($_SESSION['solution']); $i++) {

            //lettre actuelle

            if (strtoupper($_GET['mot']) == strtoupper($_SESSION['solution'][$i])) // il faudrait tester les accents

            {

                echo $_SESSION['solution'][$i];

                $au_moins_une_lettre_trouvee = true;
            } else {

                //lettres précédentes

                $lettre_trouvee = false;

                for ($j = 1; $j < count($_SESSION['deja_dites']); $j++)

                    if (strtoupper($_SESSION['deja_dites'][$j]) == strtoupper($_SESSION['solution'][$i])) {

                        echo $_SESSION['solution'][$i];

                        $lettre_trouvee = true;

                        break;
                    }

                if (!$lettre_trouvee) {

                    echo "_";

                    $_SESSION['mot_trouve'] = false;
                }
            }
        }

        //si on tape le mot en entier

        if (strtoupper($_GET['mot']) == strtoupper($_SESSION['solution']))

            $_SESSION['mot_trouve'] = true;



        //affiche les lettres déja dites

        echo "<p>";

        for ($i = 1; $i < count($_SESSION['deja_dites']); $i++) {

            echo $_SESSION['deja_dites'][$i];

            echo "<p> ";
        }

        echo "</p>";



        //compteur d'echecs

        if (!isset($_SESSION['nb_echecs'])) {

            $_SESSION['nb_echecs'] = 0;
        }

        if (!$au_moins_une_lettre_trouvee) {

            $_SESSION['nb_echecs'] += 1;
        }

        echo '<p>erreurs : ' . $_SESSION["nb_echecs"] . '/10</p>';





        switch ($_SESSION["nb_echecs"]) {

            case 9:

                echo "<p>    ___</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |   O</p>";

                echo "<p>   |  _|_</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |  / \\</p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 8:

                echo "<p>    ___</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |   O</p>";

                echo "<p>   |  _|_</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |  /  </p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 7:

                echo "<p>    ___</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |   O</p>";

                echo "<p>   |  _|_</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |    </p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 6:

                echo "<p>    ___</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |   O</p>";

                echo "<p>   |  _| </p>";

                echo "<p>   |   |</p>";

                echo "<p>   |    </p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 5:

                echo "<p>    ___</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |   O</p>";

                echo "<p>   |   | </p>";

                echo "<p>   |   |</p>";

                echo "<p>   |    </p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 4:

                echo "<p>    ___</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |   O</p>";

                echo "<p>   |     </p>";

                echo "<p>   |    </p>";

                echo "<p>   |    </p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 3:

                echo "<p>    ___</p>";

                echo "<p>   |   |</p>";

                echo "<p>   |    </p>";

                echo "<p>   |     </p>";

                echo "<p>   |    </p>";

                echo "<p>   |    </p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 2:

                echo "<p>    ___</p>";

                echo "<p>   |    </p>";

                echo "<p>   |    </p>";

                echo "<p>   |     </p>";

                echo "<p>   |    </p>";

                echo "<p>   |    </p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 1:

                echo "<p>       </p>";

                echo "<p>   |    </p>";

                echo "<p>   |    </p>";

                echo "<p>   |     </p>";

                echo "<p>   |    </p>";

                echo "<p>   |    </p>";

                echo "<p>___|___</p>";

                echo "<p></p>";

                break;

            case 0:

                echo "<p>       </p>";

                echo "<p>        </p>";

                echo "<p>        </p>";

                echo "<p>         </p>";

                echo "<p>        </p>";

                echo "<p>        </p>";

                echo "<p>_______</p>";

                echo "<p></p>";

                break;

            default:

                echo "<p>erreur dessin pendu";

                break;
        }



        //in

        if ($_SESSION['mot_trouve'] || $_SESSION['nb_echecs'] >= 10) {

            header('Location: final.php?traitement=OK');

            exit(0);
        }
    }

    ?>

</body>

</html>
