<html>

<head>

    <link rel="stylesheet" type="text/css" href="./style.css">

</head>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=latin-1" />

</head>

<body>

    <?php require 'header.php';

    if (isset($_SESSION['id']))

        $_SESSION['id']++;

    else

        $_SESSION['id'] = 0;

    if ($_SESSION['mot_trouve'] >= 10) {

        echo ' <h1>Gagné !!</h1>';

        $sql = "insert into partie values(" . $_SESSION['id'] . ", '" . $_SESSION['joueur1'] . "', '" . $_SESSION['joueur2'] . "', '" . $_SESSION['solution'] . "', 2, " . $_SESSION['nb_essais'] . ");";
    } else {

        echo '<h1>Perdu !!!</h1>';

        $sql = "insert into partie values(" . $_SESSION['id'] . ", '" . $_SESSION['joueur1'] . "', '" . $_SESSION['joueur2'] . "', '" . $_SESSION['solution'] . "', 1, " . $_SESSION['nb_essais'] . ");";
    }

    unset($_SESSION['deja_dites']);

    $coups = $_SESSION['nb_essais'];

    $_SESSION['nb_essais'] = 0; //on reset le compteur ( à 0 car comme il est déjà set il sera directement incrémenté )

    $_SESSION['nb_echecs'] = 0;





    try {

        $connexion = new PDO("mysql:host=$hostname;dbname=$database", $username, $password);

        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $connexion->query($sql);

        echo "<p>" . $_SESSION['solution'] . " : " . $coups . " coups\n</p>";
    } catch (PDOException $e) {

        die('Erreur PDO : ' . $e->getMessage());
    } catch (Exception $e) {

        die('Erreur générale : ' . $e->getMessage());
    }

    ?>

    <button><a href="index.php">Accueil</a></button>

    <button><a href="new-partie.php">Rejouer</a></button>

</body>

</html>
